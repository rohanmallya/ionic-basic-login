import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { GooglePlus } from 'ionic-native';
import { GoHerePage } from '../go-here/go-here';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
 
    constructor(public navCtrl: NavController) {
 
    }
 
    login(){
 
        GooglePlus.login({
          'webClientId': '1051177498490-2spbks8n4khiachddgidrqvr58f6hnc8.apps.googleusercontent.com'
        }).then((res) => {
            console.log(res);
            var p = "OP"
            this.navCtrl.push(GoHerePage,{res:res});
        }, (err) => {
            console.log(err);
        });
 
    }
 
    logout(){
 
        GooglePlus.logout().then(() => {
            console.log("logged out");
        });
 
    }
 
}