import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the GoHerePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-go-here',
  templateUrl: 'go-here.html',
})
export class GoHerePage {
  res: String;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.res = navParams.get('res');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GoHerePage');
  }

}
